# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"


# PSEUDOCODE
# output: list of gear needed for the day based on conditions
# conditions:
# 1. if the day is not sunny AND it is a workday, list needs umbrella
# 2. if the day is a workday, list needs laptop
# 3. if the day is NOT a workday, list needs surfboard


def gear_for_day(is_workday, is_sunny):
    list_of_needed_items = []
    if is_workday == "Yes" and is_sunny == "No":
        list_of_needed_items.append("umbrella")
    if is_workday == "Yes":
        list_of_needed_items.append("laptop")
    if is_workday == "No":
        list_of_needed_items.append("surfboard")
    return list_of_needed_items


day1_gear = gear_for_day("Yes", "Yes")
day2_gear = gear_for_day("Yes", "No")
day3_gear = gear_for_day("No", "No")
day4_gear = gear_for_day("No", "Yes")
print(day1_gear)
print(day2_gear)
print(day3_gear)
print(day4_gear)

# *** solutions are different; we have basically the same stuff
# but parameters seem to be different; i'm using Y/N or T/F but
# that doesn't seem to work as arguments in their solution
