# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

# PSEUDOCODE
# input = list of numbers
# output = avaerage of numbers in list
# conditions:
# if list is empty return None

def calculate_average(values):
    total = 0
    if len(values) == 0:
        return "None"
    for value in values:
        total += value
        average = total/len(values)
    return average


numbers1 = [2, 3, 4, 5, 6, 7]
numbers1_avg = calculate_average(numbers1)
print(numbers1_avg)

empty_list = []
empty_list_avg = calculate_average(empty_list)
print(empty_list_avg)
