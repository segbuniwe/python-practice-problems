# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


# Pseudocode
# if person_age is >= 18 OR person has a signed consent form (Y or N),
# then tell person that they can can_skydive
# otherwise they cannot skydive


def can_skydive(age, has_consent_form):
    if age >= 18 or has_consent_form == "Yes":
        print("You can skydive!")
    else:
        print("Sorry, you cannot skydive. You must be 18+ or have consent.")


terry = can_skydive(21, "Yes")
print(terry)
sophie = can_skydive(15, "Yes")
print(sophie)
dan = can_skydive(15, "No")
print(dan)
larry = can_skydive(25, "No")
print(larry)
