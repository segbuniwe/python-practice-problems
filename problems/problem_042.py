# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.

# PSEUDOCODE
# input =  2 lists of the same length
# output = new list that has the sum of the corresponding entries(same index)
# loop through indices in 2 lists
# add each value from list1[index] and list2[index] to sum counter
# append sum to list
# return list of sum

def pairwise_add(list1, list2):
    empty_list = []
    for index in range(0, len(list1)) and range(0, len(list2)):
        indexed_sum = list1[index] + list2[index]
        empty_list.append(indexed_sum)
    return empty_list


list1 = [1, 2, 3, 4]
list2 = [4, 5, 6, 7]
print(pairwise_add(list1, list2))
list3 = [100, 200, 300]
list4 = [10, 1, 180]
print(pairwise_add(list3, list4))
