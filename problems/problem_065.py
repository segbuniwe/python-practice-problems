# Write a function that meets these requirements.
#
# Name:       biggest_gap
# Parameters: a list of numbers that has at least
#             two numbers in it
# Returns:    the largest gap between any two
#             consecutive numbers in the list
#             (this will always be a positive number)
#
# Examples:
#     * input:  [1, 3, 5, 7]
#       result: 2 because they all have the same gap
#     * input:  [1, 11, 9, 20, 0]
#       result: 20 because from 20 to 0 is the biggest gap
#     * input:  [1, 3, 100, 103, 106]
#       result: 97 because from 3 to 100 is the biggest gap
#
# You may want to look at the built-in "abs" function

# PSEUDOCODE
# input = list of numbers
# output = single integer that tells which consecutive numbers have the biggest gap
# loop through list of numbers at indices
# if number[index] > number[index + 1] return number[index +1] - number[index]

def biggest_gap(numbers):
    if len(numbers) < 2:
        return None
    empty_list = []
    for index in range(0, len(numbers)):
        if index < (len(numbers) - 1):
            gap = abs((numbers[index + 1] - numbers[index]))
            empty_list.append(gap)
            biggest_gap = max(empty_list)
    return biggest_gap


print(biggest_gap([1, 3, 5, 7]))
print(biggest_gap([1, 11, 9, 20, 0]))
print(biggest_gap([1, 3, 100, 103, 106]))
