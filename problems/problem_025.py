# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

# PSEUDOCODE
# input = list of numbers
# output = sum of each number in list (loop)
# conditions:
# if list if empty return None


def calculate_sum(values):
    total = 0
    if len(values) == 0:
        return "None"
    for value in values:
        total += value
    return total


numbers1 = [1, 2, 3, 4, 5, 6, 7]
numbers1_sum = calculate_sum(numbers1)
print(numbers1_sum)

empty_list = []
empty_list_sum = calculate_sum(empty_list)
print(empty_list_sum)
