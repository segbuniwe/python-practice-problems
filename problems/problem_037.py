# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

# PSEUDOCODE
# input =
# number (integer), number of characters in result (integer), and padding character (string)
# output = returns string that is desired number length by adding padding character to left of it
# set empty string
# number is the initial
# length of new string equal to number of characters
# add padding to left of number -> add to 0 index
# return empty/new string


def pad_left(number, length, pad):
    empty_string = ""
    empty_string += str(number)
    while len(empty_string) < length:
        empty_string = pad + empty_string
    return empty_string


print(pad_left(10, 4, "*"))
print(pad_left(10, 5, "0"))
print(pad_left(1000, 3, "0"))
print(pad_left(19, 5, " "))
