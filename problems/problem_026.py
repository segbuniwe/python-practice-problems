# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average


# PSEUDOCODE
# input = list of numbers 0-100
# find average of numbers
# output = str of letter grade based off average of numbers in list (loop)
# conditions:
# each letter grade has range the average must fall between (>,<,<=,>=)

def calculate_grade(values):
    total = 0
    for value in values:
        total += value
        average = total/len(values)
    if average >= 90:
        return "Your letter grade is an A."
    elif average >= 80 and average < 90:
        return "Your letter grade is an B."
    elif average >= 70 and average < 80:
        return "Your letter grade is an C."
    elif average >= 60 and average < 70:
        return "Your letter grade is an D."
    else:
        return "Your letter grade is an F."


math_grades = [98, 87, 88, 92]
math_letter_grade = calculate_grade(math_grades)
english_grades = [100, 98, 88, 100]
english_letter_grade = calculate_grade(english_grades)
chem_grades = [78, 67, 89, 67]
chem_letter_grade = calculate_grade(chem_grades)
print(math_letter_grade)
print(english_letter_grade)
print(chem_letter_grade)
