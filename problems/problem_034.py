# Complete the count_letters_and_digits function which
# accepts a parameter s that contains a string and returns
# two values, the number of letters in the string and the
# number of digits in the string
#
# Examples:
#   * "" returns 0, 0
#   * "a" returns 1, 0
#   * "1" returns 0, 1
#   * "1a" returns 1, 1
#
# To test if a character c is a digit, you can use the
# c.isdigit() method to return True of False
#
# To test if a character c is a letter, you can use the
# c.isalpha() method to return True of False
#
# Remember that functions can return more than one value
# in Python. You just use a comma with the return, like
# this:
#      return value1, value2

# PSEUDOCODE
# input = string
# ouput = 2 values (2 integers):
# number of letters(strings) in string and number of digits(integers/floats) in string
# loop through string
# check conditions
# if character in string is a letter, add 1 to letter counter
# if character in string is a digit, add 1 to digit counter

def count_letters_and_digits(s):
    digit_counter = 0
    letter_counter = 0
    for character in s:
        if character.isdigit() is True:
            digit_counter += 1
        if character.isalpha() is True:
            letter_counter += 1
    return letter_counter, digit_counter


print(count_letters_and_digits(""))
print(count_letters_and_digits("a"))
print(count_letters_and_digits("1"))
print(count_letters_and_digits("1a"))
