# Write a function that meets these requirements.
#
# Name:       specific_random
# Parameters: none
# Returns:    a random number between 10 and 500, inclusive,
#             that is divisible by 5 and 7
#
# Examples:
#     * returns: 35
#     * returns: 105
#     * returns: 70
#
# Guidance:
#   * Generate all the numbers that are divisible by 5
#     and 7 into a list
#   * Use random.choice to select one


import random


def specific_random():
    empty_list_5_7 = []
    list_of_numbers = range(10, 501)
    for number in list_of_numbers:
        if number % 5 == 0 and number % 7 == 0:
            empty_list_5_7.append(number)
    return random.choice(empty_list_5_7)


print(specific_random())
