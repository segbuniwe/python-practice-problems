# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]


def halve_the_list(list):
    new_list_1 = []
    new_list_2 = []
    if len(list) % 2 == 0:
        divided_list_number = len(list) // 2
        for index in range(0, divided_list_number):
            new_list_1.append(list[index])
        for index in range(divided_list_number, len(list)):
            new_list_2.append(list[index])
    if len(list) % 2 != 0:
        odd_division = (len(list) // 2) + 1
        for index in range(0, odd_division):
            new_list_1.append(list[index])
        for index in range(odd_division, len(list)):
            new_list_2.append(list[index])
    return new_list_1, new_list_2


print(halve_the_list([1, 2, 3, 4]))
print(halve_the_list([1, 2, 3]))
