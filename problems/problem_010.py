# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# Psuedocode
# if value is divivisble by 3 ( value % 3 == 0),
# return the word "fizz"
# otherwise return number

def is_divisible_by_3(number):
    if number % 3 == 0:
        return "fizz"
    else:
        return number


thirty = is_divisible_by_3(30)
twenty = is_divisible_by_3(20)
twenty_seven = is_divisible_by_3(27)
print(thirty)
print(twenty)
print(twenty_seven)
