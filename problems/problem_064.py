# Write a function that meets these requirements.
#
# Name:       temperature_differences
# Parameters: highs: a list of daily high temperatures
#             lows: a list of daily low temperatures
# Returns:    a new list containing the difference
#             between each high and low temperature
#
# The two lists will be the same length
#
# Example:
#     * inputs:  highs: [80, 81, 75, 80]
#                lows:  [72, 78, 70, 70]
#       result:         [ 8,  3,  5, 10]


def temperature_differences(highs, lows):
    empty_list = []
    for index in range(0, len(highs)) and range(0, len(lows)):
        difference = highs[index] - lows[index]
        empty_list.append(difference)
    return empty_list


highs1 = [80, 81, 75, 80]
lows1 = [72, 78, 70, 70]
print(temperature_differences(highs1, lows1))
