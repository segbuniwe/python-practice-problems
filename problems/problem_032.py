# Complete the sum_of_first_n_numbers function which accepts
# a numerical limit and returns the sum of the numbers from
# 0 up to and including limit.
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+1=1
#   * 2 returns 0+1+2=3
#   * 5 returns 0+1+2+3+4+5=15
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# PSEUDOCODE
# input = numerical limit (integer?)
# output = sum of numbers from 0 up to and including that limit
# use range () to set lower and upper limit
# loop through each value and add to sum counter
# return final sum
# if the limit is < 0 return none

def sum_of_first_n_numbers(limit):
    sum = 0
    if limit < 0:
        return "None"
    for value in range(0, (limit+1)):
        sum += value
    return sum


print(sum_of_first_n_numbers(-1))
print(sum_of_first_n_numbers(1))
print(sum_of_first_n_numbers(2))
print(sum_of_first_n_numbers(5))
