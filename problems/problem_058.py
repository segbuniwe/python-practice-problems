# Write a function that meets these requirements.
#
# Name:       group_cities_by_state
# Parameters: a list of cities in the format "«name», «st»"
#             where «name» is the name of the city, followed
#             by a comma and a space, then the two-letter
#             abbreviation of the state
# Returns:    a dictionary whose keys are the two letter
#             abbreviations of the states in the list and
#             whose values are a list of the cities appearing
#             in that list for that state
#
# In the items in the input, there will only be one comma.
#
# Examples:
#     * input:   ["San Antonio, TX"]
#       returns: {"TX": ["San Antonio"]}
#     * input:   ["Springfield, MA", "Boston, MA"]
#       returns: {"MA": ["Springfield", "Boston"]}
#     * input:   ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]
#       returns: {"OH": ["Cleveland", "Columbus"], "IL": ["Chicago"]}
#
# You may want to look up the ".strip()" method for the string.


# PSEUDOCODE
# input = list of cities with city name and state abbreviation separated by space and comma
# output = dictionary with key being state abbreviation and values being cities


# def group_cities_by_states(cities):
#     city_state_list = []
#     city_names = []
#     state_names = []
#     for city in cities:
#         stripped = city.strip()
#         city_state_list.append(stripped)
#         for city_state in city_state_list:
#             # empty_dict = {}
#             split = city_state.split(",")
#             slice_city = split[0]
#             slice_state = split[1]
#             if slice_city not in city_names:
#                 city_names.append(slice_city)
#                 state_names.append(slice_state)

#     # print(city_names)
#     # print(state_names)

#     dictionary = {key: value for key, value in zip(state_names, [city_names])}
#     return dictionary


# print(group_cities_by_states(["Cleveland, OH", "Columbus, OH", "Chicago, IL"]))
# print(group_cities_by_states(["San Antonio, TX"]))
# print(group_cities_by_states(["Springfield, MA", "Boston, MA"]))
# print(group_cities_by_states(["Memphis, TN", "Nashville, TN", "Knoxville, TN", "Chattanooga, TN"]))


def group_cities_by_state(cities):          # solution
    output = {}                             # solution
    for city in cities:                     # solution
        name, state = city.split(",")    # solution
        state = state.strip()             # solution
        if state not in output:             # solution
            output[state] = []              # solution
        output[state].append(name)          # solution
    return output


print(group_cities_by_state(["Cleveland, OH", "Columbus, OH", "Chicago, IL"]))
print(group_cities_by_state(["San Antonio, TX"]))
print(group_cities_by_state(["Springfield, MA", "Boston, MA"]))
print(group_cities_by_state(["Memphis, TN", "Nashville, TN", "Knoxville, TN", "Chattanooga, TN"]))
