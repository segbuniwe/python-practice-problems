# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


# PSEUDOCODE
# if list has "flour", "eggs", and "oil" ("flour", "eggs", & "oil" in list)
# return True
# otherwise return False

def can_make_pasta(ingredients):
    if "flour" and "eggs" and "oil" in ingredients:
        return True
    else:
        return False


cake = can_make_pasta(["flour", "baking soda", "eggs", "oil", "vanilla"])
tomato = can_make_pasta(["red tomato", "green tomato"])
print(cake)
print(tomato)
