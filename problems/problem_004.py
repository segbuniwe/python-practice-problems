# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


# Pseudocode:
# given 3 values: x, y, and z
# find maximum value between x, y, and z
# if 2 values from x, y, and z are equal, return either of the 2 equal values
# if all values x, y, and z are equal, return any of them


def max_of_three(value1, value2, value3):
    # maximum_value = max(value1, value2, value3)
    if value1 >= value2 and value1 >= value3:
        return value1
    elif value2 >= value3 and value2 >= value1:
        return value2
    elif value3 >= value2 and value3 >= value1:
        return value3


three_nums = max_of_three(4, 5, 6)
print(three_nums)
same_nums = max_of_three(3, 3, 3)
print(same_nums)
two_same = max_of_three(2, 2, 1)
print(two_same)
