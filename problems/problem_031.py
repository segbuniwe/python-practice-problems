# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# PSEUDOCODE
# input = list of numbers
# output = sum of each number in list squared (integer)
# loop through each number in list
# square each number
# add squared values to a Zero
# return summed value


def sum_of_squares(values):
    sum = 0
    if len(values) == 0:
        return "None"
    for value in values:
        squared_value = value**2
        sum += squared_value
    return sum


numbers = [1, 2, 3, 4, 5]
numbers_squared_sum = sum_of_squares(numbers)
print(numbers_squared_sum)
print(sum_of_squares([]))
