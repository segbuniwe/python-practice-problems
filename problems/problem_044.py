# Complete the translate function which accepts two
# parameters, a list of keys and a dictionary. It returns a
# new list that contains the values of the corresponding
# keys in the dictionary. If the key does not exist, then
# the list should contain a None for that key.
#
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.

# PSEUDOCODE
# input = 2 inputs: list of keys and a dictionary
# output = new list that has corresponding value of key
# if key doesn't exist then list should have none
# make empty list
# loop through key_list
# if key_list value == Key, append value to list
# else append "none"
# return new list

def translate(key_list, dictionary):
    new_list = []
    for key_name in key_list:
        if key_name in dictionary:
            new_list.append(dictionary.get(key_name))
        else:
            new_list.append("None")
    return new_list


keys1 = ["name", "age"]
dictionary1 = {"name": "Noor", "age": 29}
print(translate(keys1, dictionary1))
keys2 = ["eye color", "age"]
dictionary2 = {"name": "Noor", "age": 29}
print(translate(keys2, dictionary2))
keys3 = ["age", "age", "age"]
dictionary3 = {"name": "Noor", "age": 29}
print(translate(keys3, dictionary3))
