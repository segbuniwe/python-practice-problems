# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.


# PSEDUOCODE
# check if number of people in list is >= half the number of people in 2nd list

def has_quorum(attendees_list, members_list):
    if len(attendees_list) >= (0.50) * len(members_list):
        return "The attendees list is greater than or equal to half of the members list."
    else:
        return "The attendees list is NOT greater than or equal to half of the members list."


attendees = ["Sheryl", "Quinta", "Tyler", "Lisa", "Janelle", "Chris"]
members = ["Dave", "Peter", "Homer", "Stan", "Bob", "Francine", "Linda", "Lois", "Quagmire", "Cleavland", "Joe", "Bonnie", "Chris", "Meg", "Stewie", "Brian", "Tina", "Louise", "Gene"]
quorum1 = has_quorum(attendees, members)
print(quorum1)
