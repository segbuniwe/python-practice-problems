# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.


# PSEDUCODE
# have 2 values: x and y
# checks if x is between 0-10; return "yes" if so
# checks if y is between 0-10; return "yes" if so

def is_inside_bounds(x, y):
    boundary = range(0, 11)
    if x in boundary and y in boundary:
        return "x and y is between 0 and 10."
    elif x in boundary:
        return "only x is between 0 and 10."
    elif y in boundary:
        return "only y is between 0 and 10."
    else:
        return "neither x or y is between 0 and 10."


both_in_boundary = is_inside_bounds(9, 4)
both_out_boundary = is_inside_bounds(15, 25)
x_in = is_inside_bounds(3, 20)
y_in = is_inside_bounds(17, 5)
print(both_in_boundary)
print(both_out_boundary)
print(x_in)
print(y_in)
