# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.


# PSEUDOCODE
# input = string
# output = a string with any duplicate removed
# loop through string and use string method to remove duplicates
def remove_duplicate_letters(s):
    if len(s) == 0:
        return ""
    # empty_str = ""
    for letter in s:
        letter_count = s.count(letter)
        # print(letter_count)
        if letter_count <= 1:
            return s
        elif letter_count > 1:
            return "".join(sorted(set(s)))


abc_ex = remove_duplicate_letters("abc")
abc1_ex = remove_duplicate_letters("abcabc")
abc2_ex = remove_duplicate_letters("abccbad")
abc3_ex = remove_duplicate_letters("")
print(abc_ex)
print(abc1_ex)
print(abc2_ex)
print(abc3_ex)
