# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

# PSEUDOCODE
# input = list of numbers
# output = maximum value in list
# condition:
# if list is empty return None

def max_in_list(values):
    if len(values) == 0:
        return "None"
    else:
        maximum_value = max(values)
    return maximum_value


numbers = [1, 2, 3, 4, 5, 6]
max_in_numbers = max_in_list(numbers)
print(max_in_numbers)

empty_list = []
max_in_empty = max_in_list(empty_list)
print(max_in_empty)
