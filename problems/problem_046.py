# Complete the make_sentences function that accepts three
# lists.
#   * subjects contains a list of subjects for three-word sentences
#   * verbs contains a list of verbs for three-word sentences
#   * objects contains a list of objects for three-word sentences
# The make_sentences function should return all possible sentences
# that can be made from the words in each list
#
# Examples:
#   * subjects: ["I"]
#     verbs:    ["play"]
#     objects:  ["Portal"]
#     returns:  ["I play Portal"]
#   * subjects: ["I", "You"]
#     verbs:    ["play"]
#     objects:  ["Portal", "Sable"]
#     returns:  ["I play Portal", "I play Sable",
#                "You play Portal", "You play Sable"]
#   * subjects: ["I", "You"]
#     verbs:    ["play", "watch"]
#     objects:  ["Portal", "Sable"]
#     returns:  ["I play Portal", "I play Sable",
#                "I watch Portal", "I watch Sable",
#                "You play Portal", "You play Sable"
#                "You watch Portal", "You watch Sable"]
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

# PSEUDOCODE
# input = 3 lists with subjects, verbs, objects
# output = all possible sentence structures (strings)
# make empty list
# loop through each subject, verb, object
# add them together to make sentences

def make_sentences(subjects, verbs, objects):
    empty_list = []
    for subject in subjects:
        for verb in verbs:
            for object in objects:
                empty_list.append(f"{subject} {verb} {object}")
    return empty_list


subjects1 = ["I"]
verbs1 = ["play"]
objects1 = ["Portal"]
print(make_sentences(subjects1, verbs1, objects1))
subjects2 = ["I", "You"]
verbs2 = ["play"]
objects2 = ["Portal", "Sable"]
print(make_sentences(subjects2, verbs2, objects2))
subjects3 = ["I", "You"]
verbs3 = ["play", "watch"]
objects3 = ["Portal", "Sable"]
print(make_sentences(subjects3, verbs3, objects3))
