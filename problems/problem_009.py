# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


# Pseudocode:
# have one word
# Check if word is equal when read forward and backword:
# so if word[index] == word[-index] then it is a palindrome
# otherwise it is not a palindrome


def is_palindrome(word):
    for index in range(0, len(word)):
        if word[index] == word[-1 - index]:
            return "This word, " + word + ", is a palindrome."
        else:
            return "This word, " + word + ", is NOT a palindrome!"


civic = is_palindrome("civic")
print(civic)
cycle = is_palindrome("cycle")
print(cycle)
