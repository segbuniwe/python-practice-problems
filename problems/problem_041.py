# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

# PSEUDOCODE
# input =  list of string of numbers
# output =  sum of numbers in list
# split string into individual string numbers
# turn string numbers to integers
# add each number to sum which was set equal to 0
# return sum of each string

def add_csv_lines(csv_lines):
    if len(csv_lines) == 0:
        return []
    new_list = []
    for string in csv_lines:
        if len(string) == 1:
            new_list.append(int(string))
        elif len(string) > 1:
            split_string = string.split(",")
            sum = 0
            for number_str in split_string:
                int_number = int(number_str)
                sum += int_number
            new_list.append(sum)
    return new_list


print(add_csv_lines([]))
print(add_csv_lines(["3", "1,9"]))
print(add_csv_lines(["8,1,7", "10,10,10", "1,2,3"]))
print(add_csv_lines(["8,1,7", "10,10,10", "1,2,3", "2,3,4,5"]))
