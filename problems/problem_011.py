# Complete the is_divisible_by_5 function to return the
# word "buzz" if the value in the number parameter is
# divisible by 5. Otherwise, just return the number.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# Pseudocode
# if value % 5 == 0 return "buzz"
# otherwise return value

def is_divisible_by_5(number):
    if number % 5 == 0:
        return "buzz"
    else:
        return number


fifty = is_divisible_by_5(50)
twenty_seven = is_divisible_by_5(27)
thirty = is_divisible_by_5(30)
print(fifty)
print(twenty_seven)
print(thirty)
