# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem


def shift_letter(word):
    alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    new_word = ""
    new_letter = ""
    for letter in word:
        if letter == "Z":
            new_letter = "A"
            new_word += new_letter
        elif letter == "z":
            new_letter = "a"
            new_word += new_letter
        else:
            for index in range(0, len(alphabet)):
                if letter == alphabet[index]:
                    new_letter = alphabet[index + 1]
                    new_word += new_letter
    return new_word


print(shift_letter("import"))
print(shift_letter("ABBA"))
print(shift_letter("Kala"))
print(shift_letter("zap"))
