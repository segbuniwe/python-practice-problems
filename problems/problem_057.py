# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

# PSEUDOCODE
# goes up to number + 1
# where you are dividing each number by the next number up to number + 1
# return sum of those fractions
# list from 1 to number + 1
# index = 0
# while index < len(list)
# fraction = list[index]/list[index + 1]
# sum += fraction


def sum_fraction_sequence(number):
    new_list = list(range(1, number + 2))
    # print(new_list)
    sum = 0
    for index in range(0, len(new_list)):
        if index < (len(new_list) - 1):
            fraction = new_list[index] / new_list[index + 1]
            sum += fraction
            total_sum = sum
    return total_sum


print(sum_fraction_sequence(3))
print(sum_fraction_sequence(2))
print(sum_fraction_sequence(1))
