# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


# Pseudocode:
# 2 values = [x, y]
# find minimum_value between values x and y
# if values x and y are equal, return either x or y


def minimum_value(value1, value2):
    minimum_val = min(value1, value2)
    if value1 == value2:
        return value1
    else:
        return minimum_val


two_three = minimum_value(2, 3)
five_ten = minimum_value(5, 10)
print(two_three)
print(five_ten)
