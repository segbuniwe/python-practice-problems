# Complete the find_indexes function which accepts two
# parameters, a list and a search term. It returns a new
# list that contains the indexes of the search term in
# the search list.
#
# Remember that indexes in Python are zero-based. That
# means the first element in the list is index 0.
#
# Examples:
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  4
#     result:       [3]
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  6
#     result:       []
#   * search_list:  [1, 2, 1, 2, 1]
#     search_term:  1
#     result:       [0, 2, 4]
#
# Look up the enumerate function to help you with this problem.

# PSEUDOCODE
# input = list and search item(integer)
# output = new_list that has indexes of search item in list
# loop through list of numbers by index
# if search item in list append index to empty list
# if not return empty list


def find_indexes(search_list, search_term):
    new_list = []
    for index, item in enumerate(search_list, 0):
        if item == search_term:
            new_list.append(index)
    return new_list

    # for index in range(0, len(search_list)):
    #     if search_term in search_list:
    #         new_list.append(index)
    #         return new_list
    #     else:
    #         return []


search_list1 = [1, 2, 3, 4, 5]
search_term1 = 4
print(find_indexes(search_list1, search_term1))
search_list2 = [1, 2, 3, 4, 5]
search_term2 = 6
print(find_indexes(search_list2, search_term2))
search_list3 = [1, 2, 1, 2, 1]
search_term3 = 1
print(find_indexes(search_list3, search_term3))
